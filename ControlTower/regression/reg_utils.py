from data_utils import data_format as du
from regression import regression_model as rm
import numpy as np

def apply_model(data,modelType='reg'):
    '''
    Apply the regression model function on unpivoted (but filtered) data
    :param data:
    :param modelType:
    :return:
    '''
    data = du.regression_filt(data)
    data = du.pivot(data)
    data = du.remove_outliers(data)
    model = rm.regModel(data)
    model.calculateModel(modelType)
    return data_points(model),model.scatter_data(),model

def max_line(data,day=100):
    '''
    Logic to find the "best" model on one line -- currently based on r^2 and highest value on 100th day
    #TODO: find a better logic for best line
    :param data:
    :param day:
    :return:
    '''
    lines = du.get_lines(data)
    max_reg_vals = []
    r2vals = []
    for line in lines:
        filtered_data = du.line_filter(data,line)

        _,scatter,mod = apply_model(filtered_data)
        max_reg_vals.append(mod.apply(day))

        r2 = mod.evaluate()
        r2vals.append(r2)

    max_reg_vals = np.array(max_reg_vals)
    max_reg_vals /= np.amax(max_reg_vals)

    r2vals = np.array(r2vals)
    r2vals /= np.amax(r2vals)

    score = 1.5*r2vals + max_reg_vals
    idx = np.argmax(score)
    return lines[idx]
    # return r2vals,max_reg_vals,score, lines


def data_points(model, maxDays=500):
    # Find the production output of model for first x number of days
    days = range(1,maxDays+1)
    return [model.apply(i) for i in days]

def closest_day(pred,val,last_day):
    # Find closest day for a line given the latest output up to last_day
    pred = pred[:last_day]
    day = min(range(len(pred)), key=lambda i: abs(pred[i] - val))
    return (day, val - pred[day])

def predict(weeks,pred,current_day,diff,max_prod=225):
    # Predict for x number of weeks in advance
    return round(max(0,min(pred[current_day+5*weeks] + diff,max_prod)))


if __name__ == '__main__':
    from data_utils import data_format as dat
    df = dat.data_import(debug=True)
    df = dat.init_filter(df)
    df_line = dat.line_filter(df,'KSMX06')
    df = dat.line_filter(dat.pivot(df), 'KSMX05')
    df = dat.past_production(df)
    y_pred, scatter, mod = apply_model(df_line)
    # lastDay = df['numDays'].max()
    print(df)
    # print(closest_day(y_pred,101,lastDay))