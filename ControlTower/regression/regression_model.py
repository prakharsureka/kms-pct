import numpy as np
from scipy import optimize
from sklearn.metrics import r2_score

class regModel(object):

    def __init__(self,data):
        self.data = data
        pass

    def calculateModel(self,modelType = 'reg',power=3):
        '''
        Create the model based on the data
        :param modelType: reg or log
        :param power: power of polynomial (only works if modelType == reg)
        :return:
        '''
        if modelType == 'reg':
            self.params = np.polyfit(self.data['numDays'], self.data['production'], power)
            self.type = 'reg'
        elif modelType == 'log':
            self.params = np.polyfit(np.log(self.data['numDays']), self.data['production'], 1)
            self.type = 'log'


    def equation(self):
        #Returns the equation in string
        numParams = len(self.params)
        eqn = "y = "
        for i in range(numParams):
            if i == (numParams - 1):
                eqn += str(self.params[i])
            else:
                eqn += str(self.params[i]) + "x^" + str(numParams - i - 1) + " + "
        return eqn


    def max_val(self):
        #Finds the maximum of the function - only works for regression
        #Currently only finds local maximum -- do we have a better metric for best model?
        numParams = len(self.params)

        def f(x):
            ans = 0
            for i in range(numParams):
                ans += self.params[i] * x**(numParams - i - 1)
            return ans

        opt = optimize.fmin(lambda x: -f(x), 100,full_output = True, disp=False)

        return opt[0][0],-opt[1]

    def apply(self,numDays):
        #Input number of days and output production according to model
        numParams = len(self.params)
        if self.type == 'log':
            numDays = np.log(numDays)
        ans = 0
        for i in range(numParams):
            ans += self.params[i] * numDays ** (numParams - i - 1)
        return ans

    def scatter_data(self):
        return (self.data['numDays'], self.data['production'])

    def evaluate(self):
        '''
        :return: r^2 of model
        '''
        scat = self.scatter_data()
        predicted = [self.apply(i) for i in scat[0]]
        return r2_score(scat[1], predicted)

if __name__ == '__main__':
    from data_utils import data_format as dat
    df = dat.data_import()
    df = dat.init_filter(df)
    # df = dat.line_filter(df,'KSMX02')
    df = dat.pivot(df)

    l26 = regModel(df)
    l26.calculateModel()
    print(l26.max_val())
    print(l26.equation())



