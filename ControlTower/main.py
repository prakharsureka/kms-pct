# -*- coding: utf-8 -*-
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
from app import app
from apps import simulation, ct, lt, kpi

#############################################
# Layout of tabs
#############################################
app.layout = html.Div(
    [
        html.Div([

            dcc.Tabs(
                id="tabs",
                style={"height": "20", "verticalAlign": "middle"},
                children=[
                    dcc.Tab(label="Control Tower", value="ct_tab"),
                    dcc.Tab(label="KPI Analysis",value = 'kpi_tab'),
                    dcc.Tab(label="Loss Time Analytics", value="lt_tab"),
                    dcc.Tab(label="Simulation", value="sim_tab"),
                ],
                value="ct_tab",
            )

        ],
            className="row tabs_div"
        ),

        # Tab content
        html.Div(id="tab_content", className="row", style={"margin": "2% 3%"}),
    ],
    className="row",
    style={"margin": "0%"},
)


@app.callback(Output("tab_content", "children"), [Input("tabs", "value")])
def render_content(tab):
    '''
    Change page content when tab is collected
    :param tab: User selected tab
    :return: layout of page
    '''
    if tab == "sim_tab":
        return simulation.layout
    elif tab == "ct_tab":
        return ct.layout
    elif tab == "lt_tab":
        return lt.layout
    elif tab == "kpi_tab":
        return kpi.layout


if __name__ == '__main__':
    app.run_server(
        debug=True,
        host='127.0.0.1',
        port=9003
    )
