import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.figure_factory as ff
import plotly.graph_objs as go
import flask
from dateutil import parser
from flask import send_from_directory
from data_utils import data_format as dForm, date_utils as du
from app import app
from datetime import datetime, timedelta
import pandas as pd
from dash.exceptions import PreventUpdate

color_palette = ['rgb(0,138,179)', 'rgb(157,224,237)', 'rgb(226,152,21)', 'rgb(255,207,137)',
                 'rgb(96,96,96)', 'rgb(191,191,191)', 'rgb(203,34,91)', 'rgb(248,184,188)',
                 'rgb(0,88,45)', 'rgb(226,237,195)', 'rgb(100,110,172)', 'rgb(197,202,231)',
                 'rgb(0,44,119)', 'rgb(142,85,1)', ]

# Initial data load
df_load = dForm.data_import('prod', False, 'full')
df_throughput = dForm.init_filter(df_load, weekends=True)
cycle_load = dForm.data_import('cycle', False, 'full')
df_cycle = dForm.init_filter(cycle_load, weekends=True, mode='cycle')
min_date = df_throughput['date'].min()
max_date = df_throughput['date'].max()
date_diff = int(round((max_date - min_date).days / 10))
init_date = max_date - timedelta(days=10)

df_pass = dForm.init_filter(df_load, weekends=True, mode='pass')

kpi_dict = {
    'throughput': df_throughput,
    'pass': df_pass,
    'cycle': df_cycle
}

day_fmt = '%b-%d<br>(%a)'
dates = pd.bdate_range(start=min_date, end=max_date, freq=du.custom_freq())[-5:]
start = dates[0]
end = dates[-1]

#############################################
# Graphing functions
#############################################
def throughput_graph_ct(prod_data, targ):
    '''
    Plot for production throughput
    :param prod_data: past production data (pivoted)
    :param targ: user input target
    :return: figure
    '''
    data = []

    targets = pd.DataFrame({'days': dates, 'Target': targ})
    target_dates = targets['days'].dt.strftime(day_fmt)
    data.append(
        go.Scatter(x=target_dates,
                   y=targets['Target'],
                   mode='lines',
                   name='Target',
                   line=dict(
                       color='rgb(203, 34, 91)',
                       dash='dash'
                   ),
                   )
    )
    prod_data_filtered = prod_data[prod_data['date'].between(start.date(), end.date(), inclusive=True)]
    data_meets = prod_data_filtered[prod_data_filtered['production'] >= targ]
    data_below = prod_data_filtered[prod_data_filtered['production'] < targ]

    chart_meets = go.Bar(
        x=data_meets['date'].dt.strftime(day_fmt),
        y=data_meets['production'].round(0),
        name='Meets target',
        text=data_meets['production'].round(0),
        marker=dict(
            color='rgb(189,221,163)')
    )
    data.append(chart_meets)

    chart_below = go.Bar(
        x=data_below['date'].dt.strftime(day_fmt),
        y=data_below['production'].round(0),
        name='Below target',
        text=data_below['production'].round(0),
        marker=dict(
            color='rgb(248,184,188)')
    )
    data.append(chart_below)

    layout = go.Layout(
        showlegend=True,
        barmode='stack',
        xaxis=dict(
            type="category",
            tickmode='array',
            tickvals=du.tick_freq(target_dates)
        ),
        yaxis=dict(
            title='Throughput',
        ),
        autosize=True,
        margin=go.layout.Margin(
            l=50,
            r=50,
            b=40,
            t=10,
            pad=4)
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )

    return figure


def pass_rate_graph_ct(ept_data, target):
    '''
    Create plot for pass rate
    :param ept_data: pass rate from EPT (pivoted)
    :param target: user defined target
    :return: figure
    '''
    targets = pd.DataFrame({'days': dates, 'Target': target})
    data = []
    ept_data_filtered = ept_data[ept_data['date'].between(start.date(), end.date(), inclusive=True)]
    data_irm = ept_data_filtered[ept_data_filtered['harnessTypeDescription'] == 'IRM']
    data_mt = ept_data_filtered[ept_data_filtered['harnessTypeDescription'] == 'MT']

    total_dates = data_irm['date'].dt.strftime(day_fmt)
    irm_chart = go.Scatter(
        x=total_dates,
        y=data_irm['passRate'],
        name='IRM',
        mode='lines+markers',
        line=dict(
            color='rgb(0,138,179)',
        )
    )
    data.append(irm_chart)

    mt_chart = go.Scatter(
        x=total_dates,
        y=data_mt['passRate'],
        name='MT',
        mode='lines+markers',
        line=dict(
            color='rgb(226,152,21)',
        )
    )
    data.append(mt_chart)

    data.append(
        go.Scatter(x=total_dates,
                   y=targets['Target'],
                   mode='lines',
                   name='Target',
                   line=dict(
                       color='red',
                       dash='dash'
                   ),
                   )
    )

    layout = go.Layout(
        yaxis=dict(
            title='Pass Rate',
            tickformat=',.0%'
        ),
        xaxis=dict(
            type="category",
            tickmode='array',
            tickvals=du.tick_freq(total_dates)
        ),
        legend=dict(
            traceorder='reversed'
        ),
        autosize=True,
        margin=go.layout.Margin(
            l=50,
            r=50,
            b=40,
            t=10,
            pad=4)
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )

    return figure


def cycle_time_graph_ct(cycle_data, min, max):
    '''
    Plot for cycle time
    :param cycle_data: Pivoted cycle time data
    :param min: user defined min target
    :param max: user defined max target
    :return: figure
    '''
    targets = pd.DataFrame({'days': dates, 'Target_min': min, 'Target_max': max})

    data = []
    data_filtered = cycle_data[cycle_data['date'].between(start.date(), end.date(), inclusive=True)]
    data_irm = data_filtered[data_filtered['harnessTypeDescription'] == 'IRM']
    data_mt = data_filtered[data_filtered['harnessTypeDescription'] == 'MT']

    total_dates = data_irm['date'].dt.strftime(day_fmt)
    irm_chart = go.Scatter(
        x=total_dates,
        y=data_irm['mean'],
        name='IRM',
        mode='lines+markers',
        line=dict(
            color='rgb(0,138,179)',
        )
    )
    data.append(irm_chart)

    mt_chart = go.Scatter(
        x=total_dates,
        y=data_mt['mean'],
        name='MT',
        mode='lines+markers',
        line=dict(
            color='rgb(226,152,21)',
        )
    )
    data.append(mt_chart)

    data.append(
        go.Scatter(x=total_dates,
                   y=targets['Target_min'],
                   mode='lines',
                   name='Target min',
                   line=dict(
                       color='green',
                       dash='dash'
                   ),
                   )
    )

    data.append(
        go.Scatter(x=total_dates,
                   y=targets['Target_max'],
                   mode='lines',
                   name='Target max',
                   line=dict(
                       color='red',
                       dash='dash'
                   ),
                   )
    )

    layout = go.Layout(
        showlegend=True,
        xaxis=dict(
            type="category",
            tickmode='array',
            tickvals=du.tick_freq(total_dates)
        ),
        yaxis=dict(
            title='Cycle Time (min)',
        ),
        autosize=True,
        margin=go.layout.Margin(
            l=50,
            r=50,
            b=40,
            t=10,
            pad=4),
        legend=dict(
            orientation="h",
            x=0, y=1.2),
        # yaxis=dict(
        #     range=[0, 30]
        # )
    )
    figure = go.Figure(
        data=data,
        layout=layout,
    )

    return figure


#############################################
# HTML Layout of page
#############################################
layout = html.Div(children=[

    # Left Div
    html.Div([

        html.Div(children=[html.P('IRM Production Throughput', style={'textAlign': 'left',
                                                                      'font-size': 20}), ]
                 , style={'display': 'inline-block'}),
        html.Div(children=[
            dcc.Input(
                placeholder=750,
                type='number',
                value=750,
                id='prod-target-input',
                style={'width': '75px', 'height': '25px'}
            ),
        ], style={'margin': 10, 'display': 'inline-block', 'float': 'right'}
            , ),
        html.Div(children=[
            html.P('Production target', style={'textAlign': 'left'}),
        ], style={'margin': 5, 'display': 'inline-block', 'float': 'right'}
            , ),
        dcc.Graph(id='IRM-prod', style={
            'height': '20vh'}),
        html.P('MT Production Throughput', style={'textAlign': 'left',
                                                  'font-size': 20}),
        dcc.Graph(id='MT-prod', style={
            'height': '20vh'}),
    ], className='six columns', style={'margin': 0}),

    # Right Div
    html.Div([
        html.Div(children=[html.P('EPT Cycle Time', style={'textAlign': 'left',
                                                           'font-size': 20}), ]
                 , style={'display': 'inline-block'}),
        html.Div(children=[
            dcc.Input(
                placeholder=13,
                type='number',
                value=13,
                id='min-input',
                style={'width': '75px', 'height': '25px'}
            ),
            dcc.Input(
                placeholder=15,
                type='number',
                value=15,
                id='max-input',
                style={'width': '75px', 'height': '25px'}
            ),
        ], style={'margin': 10, 'display': 'inline-block', 'float': 'right'}
            , ),
        html.Div(children=[
            html.P('Cycle time target range', style={'textAlign': 'left'}),
        ], style={'margin': 5, 'display': 'inline-block', 'float': 'right'}
            , ),

        dcc.Graph(id='ept-time', style={
            'height': '20vh'}),
        html.Div(children=[html.P('Quality Pass Rate', style={'textAlign': 'left',
                                                              'font-size': 20}), ]
                 , style={'display': 'inline-block'}),
        html.Div(children=[
            dcc.Input(
                placeholder=95,
                type='number',
                value=95,
                id='pr-target',
                style={'width': '75px', 'height': '25px'}
            ),
        ], style={'margin': 10, 'display': 'inline-block', 'float': 'right'}
            , ),
        html.Div(children=[
            html.P('Pass rate target (%)', style={'textAlign': 'left'}),
        ], style={'margin': 5, 'display': 'inline-block', 'float': 'right'}
            , ),

        dcc.Graph(id='pass-rate', style={
            'height': '20vh'}),
    ], className='six columns', style={'margin': 0}),
    html.Div([
        html.Div(children=[html.P('Logistics Inventory Level', style={'textAlign': 'left',
                                                                      'font-size': 20}), ]
                 , style={'display': 'inline-block'}), ], className='six columns', style={'margin': 0}),
    html.Div([
        html.Div(children=[html.P('On-time delivery (+6 days)', style={'textAlign': 'left',
                                                                       'font-size': 20}), ]
                 , style={'display': 'inline-block', 'float':'right'}),
        html.Div(children=[html.P('Gap KS/MBUSI', style={'textAlign': 'left',
                                                         'font-size': 20}), ]
                 , style={'display': 'inline-block', }),

    ], className='six columns', style={'margin': 0}),
])


#############################################
# Interaction Between Components / Controller
#############################################

# Graph 1
@app.callback(
    [Output(component_id='IRM-prod', component_property='figure'),
     Output('MT-prod', 'figure'),
     ],
    [Input('prod-target-input', 'value'),
     ]
)
def throughput_graphs(target):
    '''
    Interaction when target is changed
    :param target: user defined target
    :return: throughput plots
    '''
    df = kpi_dict['throughput'].copy()
    df = dForm.pivot(df)
    df = df.groupby(['harnessTypeDescription', 'date'], as_index=False)['production'].sum()
    df_irm = df[df['harnessTypeDescription'] == 'IRM']
    df_mt = df[df['harnessTypeDescription'] == 'MT']

    return throughput_graph_ct(df_irm, target), \
           throughput_graph_ct(df_mt, target)


@app.callback(
    Output(component_id='ept-time', component_property='figure'),
    [Input('min-input', 'value'),
     Input('max-input', 'value'),
     ]
)
def cycle_graphs(min, max):
    '''
    Interaction when target is changed
    :param min: user defined min
    :param max: user defined max
    :return: cycle time graph
    '''
    df = kpi_dict['cycle'].copy()
    df = dForm.pivot_cycleTime(df)
    df = df.groupby(['date', 'harnessTypeDescription'], as_index=False)['count', 'sum'].sum().sort_values(by=['date'])
    df['mean'] = df['sum'] / df['count']

    return cycle_time_graph_ct(df, min, max)


@app.callback(
    Output(component_id='pass-rate', component_property='figure'),
    [Input('pr-target', 'value'),
     ]
)
def pr_graphs(target):
    '''
    Interaction when target is changed
    :param target: user defined pass rate target
    :return: pass rate graph
    '''
    df = kpi_dict['pass'].copy()
    df = dForm.pivot_ept(df)
    df = dForm.pass_rate(df)
    df = df.groupby(['date', 'harnessTypeDescription'], as_index=False)['total', 'production'].sum().sort_values(
        by=['date'])
    df['passRate'] = df['production'] / df['total']

    target = float(target) / 100

    return pass_rate_graph_ct(df, target)
