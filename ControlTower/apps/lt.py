import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.figure_factory as ff
import plotly.graph_objs as go
import flask
from dateutil import parser
from flask import send_from_directory
from data_utils import data_format as dForm, date_utils as du
from app import app
from datetime import datetime, timedelta
import pandas as pd
from dash.exceptions import PreventUpdate

color_palette = ['rgb(0,138,179)', 'rgb(157,224,237)', 'rgb(226,152,21)', 'rgb(255,207,137)',
                 'rgb(96,96,96)', 'rgb(191,191,191)', 'rgb(203,34,91)', 'rgb(248,184,188)',
                 'rgb(0,88,45)', 'rgb(226,237,195)', 'rgb(100,110,172)', 'rgb(197,202,231)',
                 'rgb(0,44,119)', 'rgb(142,85,1)', ]

# Initial data load
df_load = dForm.data_import('lossTime')
df_load['date'] = pd.to_datetime(df_load['Fecha'])
min_date = df_load['date'].min()
max_date = df_load['date'].max()

kpi_dict = {'cause': 'Descripción',
            'shift': 'Turno',
            'dept': 'Departamento responsable',
            'area': 'GrossArea'}
day_fmt = '%b-%d<br>(%a)'

areas = df_load['GrossArea'].sort_values().unique()
area_options = [{'label': v, 'value': v} for v in areas]

col = dict(zip(areas, color_palette))

#############################################
# Graphing functions
#############################################

def lt_plot(lt_data):
    '''
    Plot the total loss time per day
    :param lt_data: Pivoted loss time data
    :return:
    '''

    data = []

    for area in areas:
        df = lt_data[lt_data['GrossArea'] == area]
        chart_data = go.Bar(
            x=df['date'],
            y=df['Tiempo de paro (minutos)'],
            name=area,
            marker=dict(
                color=col[area]
            )
        )
        data.append(chart_data)

    loss_total = lt_data.groupby('date', as_index=False)['Tiempo de paro (minutos)'].sum()
    data.append(
        go.Scatter(x=loss_total['date'],
                   y=loss_total['Tiempo de paro (minutos)'],
                   mode='text',
                   hoverinfo='skip',
                   showlegend=False,
                   name='Total',
                   text=loss_total['Tiempo de paro (minutos)'],
                   textposition='top center',
                   textfont=dict(
                       family='sans serif',
                       color='black')
                   )
    )

    layout = go.Layout(
        xaxis=dict(
            tickformat=day_fmt
        ),
        yaxis=dict(
            title='Minutes',
        ),
        barmode='stack',
        #Implement click events for this graph
        clickmode="event+select",
        dragmode = 'select'
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )

    return figure


def kpi_plot(time_data, kpi):
    '''
    Plot the kpi selected (e.g. departments or reasons for loss time)
    :param time_data: Loss time data
    :param kpi: user selected KPI
    :return:
    '''
    data = [
        go.Bar(
            y=time_data[kpi],
            x=time_data['Tiempo de paro (minutos)'],
            orientation='h',
            text=time_data[kpi],
            textposition='auto',
            hoverinfo='x+y'
        )
    ]
    time_data['percent'] = time_data['Tiempo de paro (minutos)']/time_data['Tiempo de paro (minutos)'].sum()
    data.append(
        go.Scatter(y=time_data[kpi],
                   x=[0]*time_data.shape[0],
                   mode='text',
                   hoverinfo='skip',
                   showlegend=False,
                   name='Total',
                   text=[str(int(i*100)) + "%" for i in time_data['percent'].round(2)],
                   textposition='middle left',
                   textfont=dict(
                       family='sans serif',
                       color='rgb(96,96,96)',
                       size=10)
                   )

    )


    layout = go.Layout(
        xaxis=dict(
            title='Minutes',
        ),
        yaxis=dict(
            showticklabels=False,
        ),
        showlegend=False,
        margin={'l': 0},

    )
    figure = go.Figure(
        data=data,
        layout=layout,

    )

    return figure


#############################################
# HTML Layout
#############################################
layout = html.Div(children=[
    html.Div([
        dcc.RangeSlider(
            id='days',
            min=du.unixTimeMillis(min_date),
            max=du.unixTimeMillis(max_date),
            marks=du.getMarks(min_date,
                              max_date,
                              2),
            value=[du.unixTimeMillis(min_date),
                   du.unixTimeMillis(max_date)]
        ),
    ], style={'margin-bottom': 20,
              'margin-right': 50}),
    # Left Div
    html.Div([
        html.Div([
            html.H4(children='Loss Time'),
            html.Div([
                html.Div([
                    html.P('Area', style={'textAlign': 'center'}),
                    dcc.Dropdown(
                        id='area-dropdown',
                        style={'width': '275px'},
                        options=area_options,
                        value=areas,
                        searchable=False,
                        clearable=False,
                        multi=True
                    )
                ], style={'margin': 5}),
                html.Div([
                    html.P('Shift', style={'textAlign': 'center'}),
                    dcc.Dropdown(
                        id='shift-dropdown',
                        style={'width': '275px'},
                        options=[
                            {'label': 'Shift 1', 'value': 1},
                            {'label': 'Shift 2', 'value': 2},
                            {'label': 'Shift 3', 'value': 3},
                        ],
                        value=[1, 2, 3],
                        searchable=False,
                        clearable=False,
                        multi=True
                    )
                ], style={'margin': 5})
            ], style={'display': 'flex'}
            ),
            dcc.Graph(id='loss'),
        ]),
    ], className='six columns', style={'margin': 0}),

    # Right Div
    html.Div([
        html.Div([
            html.H4(children='Pareto'),
        ], style={'margin-left': 10}),
        html.Div(children=[
            html.P(children='Data', style={'textAlign': 'center'}),
            dcc.Dropdown(
                id='data-dropdown',
                options=[
                    {'label': 'Cause', 'value': 'cause'},
                    {'label': 'Department', 'value': 'dept'},
                ],
                searchable=False,
                clearable=False,
                value='cause',
                style={'width': '275px'},
            )], style={'width': 275, 'margin-left': 10}),
        dcc.Graph(id='pareto'),
    ], className='six columns'),
])


#############################################
# Interaction Between Components / Controller
#############################################


@app.callback(
    [Output(component_id='loss', component_property='figure'),
     Output('loss', 'selectedData')],
    [Input('days', 'value'),
     Input('area-dropdown', 'value'),
     Input('shift-dropdown', 'value'),
    ]
)
def update_lt(days, area, shift):
    '''
    Update the loss time graph based on user selections
    :param days: user selected dates
    :param area: User selected area
    :param shift: User selected shift
    :return:
    '''
    start = du.unixToDatetime(days[0]).date()
    end = du.unixToDatetime(days[1]).date()
    df = df_load[df_load['date'].between(start, end, inclusive=True)]
    df = df[df['GrossArea'].isin(area)]
    df = df[df['Turno'].isin(shift)].sort_values(by=['GrossArea'])
    df_days = df.groupby(['date', 'GrossArea'], as_index=False)['Tiempo de paro (minutos)'].sum()


    return lt_plot(df_days),None


@app.callback(
    Output('pareto', 'figure'),
        [Input('days', 'value'),
         Input('data-dropdown', 'value'),
         Input('area-dropdown', 'value'),
         Input('shift-dropdown', 'value'),
         Input('loss', 'selectedData')
         ]
)
def update_pareto(days, kpi, area, shift, selected):
    '''
    Update KPI graph based on user selections AND user click evens on loss time graph
    :param days: User selected days on slider
    :param kpi: User selected KPI to display
    :param area: User selected area
    :param shift: User selected shift
    :param selected: User selected data from clicking or dragging
    :return:
    '''
    start = du.unixToDatetime(days[0]).date()
    end = du.unixToDatetime(days[1]).date()
    kpi_col = kpi_dict[kpi]
    df = df_load[df_load['date'].between(start, end, inclusive=True)]
    df = df[df['GrossArea'].isin(area)]
    df = df[df['Turno'].isin(shift)].sort_values(by=['GrossArea'])
    if selected is not None:
        df['date_str'] = df['date'].dt.strftime("%Y-%m-%d")
        df['tuples'] = list(df[['date_str', 'GrossArea']].itertuples(index=False, name=None))
        filtered = [(i['x'], areas[i['curveNumber']]) for i in selected['points'] if i['curveNumber'] < len(areas)]
        df['tuples'] = df[['tuples']].isin(filtered)
        df = df[df['tuples'] == True]

    df_kpi = df.groupby(kpi_col, as_index=False)['Tiempo de paro (minutos)'].sum() \
        .sort_values(by=['Tiempo de paro (minutos)'])
    return kpi_plot(df_kpi, kpi_col)


