import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.figure_factory as ff
import plotly.graph_objs as go
import flask
from dateutil import parser
from flask import send_from_directory
from data_utils import data_format as dForm, date_utils as du
from app import app
from datetime import datetime, timedelta
import pandas as pd
from dash.exceptions import PreventUpdate

color_palette = ['rgb(0,138,179)', 'rgb(157,224,237)', 'rgb(226,152,21)', 'rgb(255,207,137)',
                 'rgb(96,96,96)', 'rgb(191,191,191)', 'rgb(203,34,91)', 'rgb(248,184,188)',
                 'rgb(0,88,45)', 'rgb(226,237,195)', 'rgb(100,110,172)', 'rgb(197,202,231)',
                 'rgb(0,44,119)', 'rgb(142,85,1)', ]

# Initial data load
df_load = dForm.data_import('prod', False, 'full')
df_throughput = dForm.init_filter(df_load, weekends=True)
targets_load = dForm.data_import('targets')
cycle_load = dForm.data_import('cycle', False, 'full')
df_cycle = dForm.init_filter(cycle_load, weekends=True, mode='cycle')
min_date = df_throughput['date'].min()
max_date = df_throughput['date'].max()
date_diff = int(round((max_date - min_date).days / 10))
init_date = max_date - timedelta(days=10)

df_pass = dForm.init_filter(df_load, weekends=True, mode='pass')
df_ept = dForm.init_filter(df_load, weekends=True, mode='ept')

kpi_dict = {
    'throughput': df_throughput,
    'ept_thrghpt': df_ept,
    'pass': df_pass,
    'cycle': df_cycle
}

day_fmt = '%b-%d<br>(%a)'

#############################################
# Graphing functions
#############################################

def throughput_graph(prod_data, start, end, kpi, lines):
    '''
    Plot the throughput for EPT or assembly
    :param prod_data: Pivoted Production data
    :param start: User selected start date on slider
    :param end: User selected end data on slider
    :param kpi: User selected KPI
    :param lines: User selected lines
    :return:
    '''
    if not lines:
        lines = []
    col = dict(zip(lines, color_palette))
    data = []

    targets = pd.DataFrame({'days': pd.bdate_range(start=start, end=end, freq=du.custom_freq())})
    targets['CW'] = pd.to_numeric(targets['days'].dt.strftime("%V"))
    targets = targets.merge(targets_load, how='left', on='CW').sort_values(by='days')

    target_dates = targets['days'].dt.strftime(day_fmt)
    data.append(
        go.Scatter(x=target_dates,
                   y=targets['Target'],
                   mode='lines+markers',
                   name='Target',
                   line=dict(
                       color='rgb(203, 34, 91)',
                       dash='dash'
                   ),
                   )
    )
    # if shift != 'all':
    #     targets['Target'] = targets['Target']/ 3
    prod_data_filtered = prod_data[prod_data['date'].between(start.date(), end.date(), inclusive=True)]
    if kpi == 'throughput':
        line_col = 'productionLine'
    else:
        line_col = 'origin'
    for l in lines:
        line_data = prod_data_filtered[prod_data_filtered[line_col] == l]

        chart_data = go.Bar(
            x=line_data['date'].dt.strftime(day_fmt),
            y=line_data['production'].round(0),
            name=l,
            marker=dict(
                color=col[l])
        )
        data.append(chart_data)

    prod_total = prod_data_filtered[prod_data_filtered[line_col].isin(lines)]
    prod_total = prod_total.groupby('date', as_index=False)['production'].sum()
    data.append(
        go.Scatter(x=prod_total['date'].dt.strftime(day_fmt),
                   y=prod_total['production'],
                   mode='text',
                   hoverinfo='skip',
                   showlegend=False,
                   name='Total',
                   text=prod_total['production'],
                   textposition='top center',
                   textfont=dict(
                       family='sans serif',
                       color='black')
                   )
    )

    layout = go.Layout(
        showlegend=True,
        barmode='stack',
        xaxis=dict(
            type="category",
            tickmode='array',
            tickvals=du.tick_freq(target_dates)
        ),
        yaxis=dict(
            title='Throughput',
            # range=[0, 1000]
        )
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )

    return figure


def pass_rate_graph(ept_data, start, end, lines):
    '''
    Plot the EPT pass rate
    :param ept_data: Pivoted EPT data
    :param start: User selected start date
    :param end: User selected end date
    :param lines: User selected lines
    :return:
    '''
    if not lines:
        lines = []
    col = dict(zip(lines, color_palette))
    data = []
    ept_data_filtered = ept_data[ept_data['date'].between(start.date(), end.date(), inclusive=True)]

    total_data = ept_data_filtered.groupby(['date'], as_index=False)['total', 'production'].sum().sort_values(
        by=['date'])
    total_data['passRate'] = total_data['production'] / total_data['total']
    total_dates = total_data['date'].dt.strftime(day_fmt)
    total_chart_data = go.Scatter(
        x=total_dates,
        y=total_data['passRate'],
        name='All tables avg.',
        mode='lines',
        line=dict(
            color='black',
            width=3,
            dash='dash'

        )
    )
    data.append(total_chart_data)
    for l in lines:
        line_data = ept_data_filtered[ept_data_filtered['origin'] == l]

        chart_data = go.Scatter(
            x=line_data['date'].dt.strftime(day_fmt),
            y=line_data['passRate'],
            name=l,
            mode='lines',
            marker=dict(
                color=col[l])
        )
        data.append(chart_data)

    layout = go.Layout(
        showlegend=True,
        yaxis=dict(
            title='Pass Rate',
            tickformat=',.0%'
        ),
        xaxis=dict(
            type="category",
            tickmode='array',
            tickvals=du.tick_freq(total_dates)
        ),
        legend=dict(
            traceorder='reversed'
        )
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )

    return figure


def cycle_time_graph(cycle_data, start, end, lines):
    '''
    Plot the cycle time
    :param cycle_data: Pivoted cycle time data
    :param start: User selected start date
    :param end: User selected end date
    :param lines: User selected lines
    :return:
    '''
    if not lines:
        lines = []
    col = dict(zip(lines, color_palette))
    data = []
    data_filtered = cycle_data[cycle_data['date'].between(start.date(), end.date(), inclusive=True)]

    total_data = data_filtered.groupby(['date'], as_index=False)['count', 'sum'].sum().sort_values(by=['date'])
    total_data['mean'] = total_data['sum'] / total_data['count']
    total_dates = total_data['date'].dt.strftime(day_fmt)
    total_chart_data = go.Scatter(
        x=total_dates,
        y=total_data['mean'],
        name='All tables avg.',
        mode='lines',
        line=dict(
            color='black',
            width=3,
            dash='dash'
        )
    )
    data.append(total_chart_data)
    
    for l in lines:
        line_data = data_filtered[data_filtered['origin'] == l]

        chart_data = go.Scatter(
            x=line_data['date'].dt.strftime(day_fmt),
            y=line_data['mean'].round(1),
            name=l,
            mode='lines',
            marker=dict(
                color=col[l])
        )
        data.append(chart_data)



    layout = go.Layout(
        showlegend=True,
        legend=dict(
            traceorder='reversed'
        ),
        xaxis=dict(
            type="category",
            tickmode='array',
            tickvals=du.tick_freq(total_dates)
        ),
        yaxis=dict(
            title='Cycle Time (minutes)',
        ),
        # yaxis=dict(
        #     range=[0, 30]
        # )
    )
    figure = go.Figure(
        data=data,
        layout=layout,
    )

    return figure

#############################################
# HTML Layout
#############################################
layout = html.Div(children=[
    html.Div([
        dcc.RangeSlider(
            id='kpi-days',
            min=du.unixTimeMillis(min_date),
            max=du.unixTimeMillis(max_date),
            marks=du.getMarks(min_date,
                              max_date,
                              date_diff),
            value=[du.unixTimeMillis(init_date),
                   du.unixTimeMillis(max_date)]
        ),
    ], style={'margin': 20}),

    # Left Div
    html.Div([
        html.Div([
            html.Div([
                html.Div([
                    html.P('KPI', style={'textAlign': 'center'}),
                    dcc.Dropdown(
                        id='kpi-dropdown_left',
                        style={'width': '150px'},
                        options=[
                            {'label': 'Line Throughput', 'value': 'throughput'},
                            {'label': 'EPT Throughput', 'value': 'ept_thrghpt'},
                            {'label': 'EPT Pass Rate', 'value': 'pass'},
                            {'label': 'EPT Cycle Time', 'value': 'cycle'},
                        ],
                        value='throughput',
                        searchable=False,
                        clearable=False
                    )], style={'margin': 5}),
                html.Div([
                    html.P('Area/Shift', style={'textAlign': 'center'}),
                    dcc.Dropdown(
                        id='harness-dropdown_left',
                        style={'width': '100px', 'margin': '1px'},
                        options=[
                            {'label': 'IRM', 'value': 'IRM'},
                            {'label': 'MT', 'value': 'MT'},
                        ],
                        value='IRM',
                        searchable=False,
                        clearable=False
                    ), dcc.Dropdown(
                        id='shift-dropdown_left',
                        style={'width': '100px', 'margin': '1px'},
                        options=[
                            {'label': 'Shift 1', 'value': 1},
                            {'label': 'Shift 2', 'value': 2},
                            {'label': 'Shift 3', 'value': 3},
                            {'label': 'All Shifts', 'value': 'all'}
                        ],
                        value='all',
                        searchable=False,
                        clearable=False)
                ], style={'margin': 5}),
                html.Div(children=[
                    html.P(style={'textAlign': 'center'}, id='line-left'),
                    dcc.Dropdown(
                        id='line-dropdown_left',
                        style={'width': '225px'},
                        searchable=False,
                        multi=True,
                        # value = lines_init
                    ),
                ], style={'margin': 5}),
            ], style={'display': 'flex'}),

        ], style={'margin': 20}),
        dcc.Graph(id='kpi-left'),
    ], className='six columns', style={'margin': 0}),

    # Right Div
    html.Div([
        html.Div([
            html.Div([
                html.Div([
                    html.P('KPI', style={'textAlign': 'center'}),
                    dcc.Dropdown(
                        id='kpi-dropdown_right',
                        style={'width': '150px'},
                        options=[
                            {'label': 'Line Throughput', 'value': 'throughput'},
                            {'label': 'EPT Throughput', 'value': 'ept_thrghpt'},
                            {'label': 'EPT Pass Rate', 'value': 'pass'},
                            {'label': 'EPT Cycle Time', 'value': 'cycle'},
                        ],
                        value='throughput',
                        searchable=False,
                        clearable=False
                    )], style={'margin': 5}),
                                html.Div([
                    html.P('Area/Shift', style={'textAlign': 'center'}),
                    dcc.Dropdown(
                        id='harness-dropdown_right',
                        style={'width': '100px', 'margin': '1px'},
                        options=[
                            {'label': 'IRM', 'value': 'IRM'},
                            {'label': 'MT', 'value': 'MT'},
                        ],
                        value='MT',
                        searchable=False,
                        clearable=False
                    ), dcc.Dropdown(
                        id='shift-dropdown_right',
                        style={'width': '100px', 'margin': '1px'},
                        options=[
                            {'label': 'Shift 1', 'value': 1},
                            {'label': 'Shift 2', 'value': 2},
                            {'label': 'Shift 3', 'value': 3},
                            {'label': 'All Shifts', 'value': 'all'}
                        ],
                        value='all',
                        searchable=False,
                        clearable=False)
                ], style={'margin': 5}),
                html.Div(children=[
                    html.P(style={'textAlign': 'center'}, id='line-right'),
                    dcc.Dropdown(
                        id='line-dropdown_right',
                        style={'width': '225px'},
                        searchable=False,
                        multi=True,
                        # value = lines_init
                    )], style={'margin': 5}),
            ], style={'display': 'flex'}),

        ], style={'margin': 20}),
        dcc.Graph(id='kpi-right'),
    ], className='six columns', style={'margin': 0}),
])


#############################################
# Interaction Between Components / Controller
#############################################

# Graph 1
@app.callback(
    [Output(component_id='kpi-left', component_property='figure'),
     Output('line-dropdown_left', 'options'),
     Output('line-left', 'children')
     ],
    [Input('kpi-days', 'value'),
     Input('kpi-dropdown_left', 'value'),
     Input('harness-dropdown_left', 'value'),
     Input('shift-dropdown_left', 'value'),
     Input('line-dropdown_left', 'value')
     ]
)
def kpi_graph_left(days, kpi, hType, shift, lineInput):
    '''
    Plot the left KPI based on user selections
    :param days: User selected dates on slider
    :param kpi: User selected KPI
    :param hType: User selected harness type
    :param shift: User selected shift
    :param lineInput: User selected lines
    :return: Left chart and lines based on chart selected
    '''
    if kpi == 'throughput':
        df = df_throughput.copy()
    elif kpi == 'ept_thrghpt':
        df = df_ept.copy()
    elif kpi == 'pass':
        df = df_pass.copy()
    else:
        df = df_cycle.copy()

    df = df[df['harnessTypeDescription'] == hType]

    if shift != 'all':
        df = dForm.shift_filter(df, shift)

    start = du.unixToDatetime(days[0])
    end = du.unixToDatetime(days[1])

    if kpi == 'throughput':
        lines = dForm.get_lines(df)
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot(df)
        return throughput_graph(df,
                                start,
                                end,
                                kpi, lineInput), options, 'Lines'
    elif kpi == 'ept_thrghpt':
        lines = dForm.get_lines(df, 'ept')
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot_ept(df)
        return throughput_graph(df,
                                start,
                                end,
                                kpi, lineInput), options, 'Tables'
    elif kpi == 'pass':
        lines = dForm.get_lines(df, 'ept')
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot_ept(df)

        df = dForm.pass_rate(df)
        return pass_rate_graph(df,
                               start,
                               end, lineInput), options, 'Tables'
    elif kpi == 'cycle':
        lines = dForm.get_lines(df, 'ept')
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot_cycleTime(df)
        return cycle_time_graph(df,
                                start,
                                end, lineInput), options, 'Tables'
    else:
        return


# Graph 2
@app.callback(
    [Output(component_id='kpi-right', component_property='figure'),
     Output('line-dropdown_right', 'options'),
     Output('line-right', 'children')
     ],
    [Input('kpi-days', 'value'),
     Input('kpi-dropdown_right', 'value'),
     Input('harness-dropdown_right', 'value'),
     Input('shift-dropdown_right', 'value'),
     Input('line-dropdown_right', 'value')
     ]
)
def kpi_graph_right(days, kpi, hType, shift, lineInput):
    '''
    Plot the right KPI based on user selections
    :param days: User selected dates on slider
    :param kpi: User selected KPI
    :param hType: User selected harness type
    :param shift: User selected shift
    :param lineInput: User selected lines
    :return: right chart and lines based on chart selected
    '''
    if kpi == 'throughput':
        df = df_throughput.copy()
    elif kpi == 'ept_thrghpt':
        df = df_ept.copy()
    elif kpi == 'pass':
        df = df_pass.copy()
    else:
        df = df_cycle.copy()

    df = df[df['harnessTypeDescription'] == hType]

    if shift != 'all':
        df = dForm.shift_filter(df, shift)

    start = du.unixToDatetime(days[0])
    end = du.unixToDatetime(days[1])

    if kpi == 'throughput':
        lines = dForm.get_lines(df)
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot(df)
        return throughput_graph(df,
                                start,
                                end,
                                kpi,
                                lineInput), options, 'Lines'
    elif kpi == 'ept_thrghpt':
        lines = dForm.get_lines(df, 'ept')
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot_ept(df)
        return throughput_graph(df,
                                start,
                                end,
                                kpi,
                                lineInput), options, 'Tables'
    elif kpi == 'pass':
        lines = dForm.get_lines(df, 'ept')
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot_ept(df)

        df = dForm.pass_rate(df)
        return pass_rate_graph(df,
                               start,
                               end,
                               lineInput), options, 'Tables'
    elif kpi == 'cycle':
        lines = dForm.get_lines(df, 'ept')
        options = [{'label': line[-2:], 'value': line} for line in lines]
        df = dForm.pivot_cycleTime(df)
        return cycle_time_graph(df,
                                start,
                                end,
                                lineInput), options, 'Tables'
    else:
        return


@app.callback(
    Output('line-dropdown_right', 'value'),
    [Input('kpi-dropdown_right', 'value'),
     Input('harness-dropdown_right', 'value')],
)
def default_line_vals_right(kpi, hType):
    '''
    If throughput is selected ,show all lines by default
    :param kpi:
    :param hType:
    :return:
    '''
    if kpi == 'throughput':
        df = df_throughput.copy()
        df = df[df['harnessTypeDescription'] == hType]
        lines = dForm.get_lines(df)
        return lines
    elif kpi == 'ept_thrghpt':
        df = df_ept.copy()
        df = df[df['harnessTypeDescription'] == hType]
        lines = dForm.get_lines(df, 'ept')
        return lines

@app.callback(
    Output('line-dropdown_left', 'value'),
    [Input('kpi-dropdown_left', 'value'),
     Input('harness-dropdown_left', 'value')],
)
def default_line_vals_left(kpi, hType):
    '''
    If throughput is selected ,show all lines by default
    :param kpi:
    :param hType:
    :return:
    '''
    if kpi == 'throughput':
        df = df_throughput.copy()
        df = df[df['harnessTypeDescription'] == hType]
        lines = dForm.get_lines(df)
        return lines
    elif kpi == 'ept_thrghpt':
        df = df_ept.copy()
        df = df[df['harnessTypeDescription'] == hType]
        lines = dForm.get_lines(df, 'ept')
        return lines

