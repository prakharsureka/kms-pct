import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.figure_factory as ff
import plotly.graph_objs as go
import flask
from dateutil import parser
from flask import send_from_directory
import regression.reg_utils as ru
import regression.regression_model as rm
from data_utils import data_format as dForm
from app import app

color_palette = ['rgb(0,138,179)','rgb(157,224,237)','rgb(226,152,21)','rgb(255,207,137)',
                 'rgb(96,96,96)','rgb(191,191,191)','rgb(203,34,91)','rgb(248,184,188)',
                 'rgb(0,88,45)','rgb(226,237,195)','rgb(100,110,172)','rgb(197,202,231)',
                 'rgb(0,44,119)','rgb(142,85,1)',]

#Initial data load
df_load = dForm.data_import('prod',False,'full')
df_load = dForm.init_filter(df_load)
targets_load = dForm.data_import('targets')

# Get all lines - will probably move to function after filtering for harnessType
lines = dForm.get_lines(df_load)
on_load_lines = [{'label':line,'value':line} for line in lines]

#############################################
# Graphing functions
#############################################

def draw_reg(pred,scatter,r2,lim=60):
    '''
    Draw regression
    :param pred: Model predicted points
    :param scatter: Scatter of the real data points
    :param r2: r^2 of the model
    :param lim: Number of points to show
    :return:
    '''
    pred = pred[0:lim]
    days = [x for x in range(1,lim+1)]
    data = [
        go.Scatter(x=days, y=pred, mode='lines',name='model',text= "r<sup>2</sup> = " + str(round(r2,2))),
        go.Scatter(x=scatter[0],y=scatter[1],mode='markers',name = 'data',opacity = 0.3)
        ]
    layout = go.Layout(
        showlegend=False,
        yaxis=dict(
            title='Production'
        ),
        xaxis=dict(
            title='Days'
        ),
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )

    return figure

def draw_pred(prod_data,weeks,pred,final_week):
    '''
    Bar graph of the predictions
    :param prod_data: Real historical production data
    :param weeks: Weeks selected by user
    :param pred: Predictions in the future
    :param final_week: Last week of real data
    :return:
    '''
    weeks = list(range(weeks[0],weeks[1]+1))
    lines = dForm.get_lines(prod_data)
    targets = targets_load[targets_load['CW'].isin(weeks)]
    col = dict(zip(lines, color_palette))

    data = []
    prod_total = [0]*(final_week - weeks[0]+1)
    pred_total = [0]*(weeks[-1]-final_week)

    for l in lines:
        line_data = prod_data[prod_data['productionLine'] == l]
        prod_data_filtered = line_data[line_data['CW'].isin(weeks)].sort_values(by=['CW'])
        if weeks[0] <= final_week:
            chart_data = go.Bar(
                x=prod_data_filtered['CW'],
                y=prod_data_filtered['production'].round(0),
                name=l,
                marker=dict(
                    color=col[l])
            )
            data.append(chart_data)
            prod_total = [x + y for x, y in zip(prod_total, prod_data_filtered['production'])]

        if weeks[-1] > final_week:
            #TODO: turn into function
            pred_weeks = [w for w in weeks if w > final_week]
            last_day = int(line_data['CW'].iloc[-1])
            last_val = int(max(line_data['production'].iloc[-10:-1]))
            cDay,diff = ru.closest_day(pred, last_val, last_day)
            prediction = [ru.predict(w-final_week, pred, cDay,diff) for w in pred_weeks]
            if weeks[0] <= final_week:
                pred_chart_data = go.Bar(
                    x=pred_weeks,
                    y=prediction,
                    name=l + " Prediction",
                    showlegend=False,
                    opacity=0.55,
                    marker=dict(
                        color=col[l],
                        line=dict(
                            color='rgb(96,96,96)',
                            width=0.5),
                    )
                )
            else:
                pred_chart_data = go.Bar(
                    x=pred_weeks,
                    y=prediction,
                    name=l,
                    opacity=0.55,
                    marker=dict(
                        color=col[l],
                        line=dict(
                            color='rgb(96,96,96)',
                            width=0.5),
                    )
                )
            pred_total = [x + y for x, y in zip(pred_total, prediction)]
            data.append(pred_chart_data)
    target_line = go.Scatter(x=targets['CW'],
                             y=targets['Target'].round(0),
                             mode='lines+markers',
                             name='Target',
                             line = dict(
                                 color = 'rgb(203, 34, 91)',
                                 dash = 'dash'
                             ),
                             )
    data.append(target_line)

    totals = prod_total + pred_total

    total_text = go.Scatter(x=weeks,
                            y = totals,
                            mode = 'text',
                            name = 'Total',
                            hoverinfo='skip',
                            showlegend=False,
                            text=[int(round(i)) for i in totals],
                            textposition='top center',
                            textfont=dict(
                                family='sans serif',
                                color='black')
                            )
    data.append(total_text)


    if (weeks[0] <= final_week) and (weeks[-1] > final_week):
        vert = [{
            'type': 'line',
            'yref': 'paper',
            'x0': final_week + .5,
            'x1': final_week + .5,
            'y0': 0,
            'y1': 1,
            'opacity':0.5,
            'line': {
                'color': 'gray',
                'dash': 'dashdot',
            }
     }]
    else:
        vert = []



    layout = go.Layout(
        showlegend=True,
        xaxis=dict(
            title='CW',
        ),
        barmode='stack',
        shapes = vert
    )
    figure = go.Figure(
        data=data,
        layout=layout
    )
    return figure

#############################################
# HTML Layout
#############################################
layout = html.Div(children=[

    #Left Div
	html.Div([
		html.Div([
			html.Div([
				html.H4(children='Production Learning Curve'),
			]),

            #Checklist to select lines
            #TODO: Add filter for harnessType
			html.Div([
                html.Div([html.P('Production',style={'textAlign': 'center'}),
                    dcc.Dropdown(
                    id='harness-dropdown',
                    style={'width': '150px'},
                    options=[
                        {'label': 'IRM', 'value': 'IRM'},
                        {'label': 'MT', 'value': 'MT'},
                    ],
                    value='IRM',
                    searchable=False,
                    clearable=False
                )]),
                html.Div([html.P('Model Type',style={'textAlign': 'center'}),
                    dcc.Dropdown(
                    id='model-dropdown',
                    style={'width': '150px'},
                    options=[
                        {'label': 'All', 'value': 'all'},
                        {'label': 'Best', 'value': 'best'},
                    ],
                    value='best',
                    searchable=False,
                    clearable=False
                )]),
                html.Div([html.P('Lines Modeled',style={'textAlign': 'center'}),
                          html.Div(id='model-container',
                                   children='', style={'width': '200px','textAlign': 'center'}
                )]),
			], style={'display':'flex','margin':25}),

		], ),


        dcc.Graph(id='reg-graph'),

	], className='six columns', style={'margin':0}),

    #Right Div
	html.Div([
        html.Div([
				html.H4(children='Production Ramp-up Prediction'),
				html.P('Prediction Timeframe (Calendar Week)', style={'margin-top':25}),
			]),

        #Slider for weeks
        html.Div([
				dcc.RangeSlider(
					id='weeks-slider',
					value=[20,30],
				),
			], style={'margin-bottom':25,'margin-top':25}),
		dcc.Graph(id='pred-graph'),
	], className='six columns', style={'margin':0}),
])


#############################################
# Interaction Between Components / Controller
#############################################

#Consider splitting into two callback functions and having the dataframe/model updated globally on input change
# E.g. have another function with a dummy output to just update the models; don't need to redraw reg graph or re filter after changing slider
@app.callback(
    [Output(component_id='reg-graph', component_property='figure'),
     Output(component_id='pred-graph', component_property='figure'),
     Output(component_id='model-container',component_property='children'),
     Output(component_id='weeks-slider', component_property='min'),
     Output(component_id='weeks-slider', component_property='max'),
     Output(component_id='weeks-slider',component_property='marks')],
    [Input('harness-dropdown', 'value'),
     Input('weeks-slider','value'),
     Input('model-dropdown','value')]
)
def load_reg_graph(hType,weeks,modelType):
    '''
    Plot graphs when dropdown or sliders are changed
    :param hType: Harness type selected (IRM or MT)
    :param weeks: Weeks selected on slider
    :param modelType: Best regression for one line or all data
    :return: figures, which lines are used in regression, what values are highlighted on slider
    '''
    bestLine = "All"
    df = df_load[df_load['harnessTypeDescription'] == hType]
    if modelType == 'best':
        bestLine = ru.max_line(df)
        df_pred = dForm.line_filter(df,bestLine)
        y_pred,scatter,mod= ru.apply_model(df_pred,'log')
        figure1 = draw_reg(y_pred,scatter,mod.evaluate(),85)

    else:
        y_pred,scatter,mod = ru.apply_model(df,'log')
        figure1 = draw_reg(y_pred,scatter,mod.evaluate(),85)

    df_pastProd = dForm.pivot(df)
    pastProd = dForm.past_production(df_pastProd)
    slide_min = pastProd['CW'].min()
    slide_max = 50
    final_week = pastProd['CW'].max()
    marks = {
        weeks[0]:{'label': f'{weeks[0]}'},
        weeks[1]:{'label': f'{weeks[1]}'}
    }

    marks[int(final_week)] = {'label': f'{final_week}', 'style': {'color': '#cb225b'}}


    figure2 = draw_pred(pastProd,weeks,y_pred,final_week)
    return figure1,figure2, bestLine,slide_min,slide_max,marks



