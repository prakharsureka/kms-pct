import datetime
import pandas as pd


#TODO: Get lines by harnessType, show all data points, what is "best" regression


def data_import(file='prod',debug=False,datatype='full'):
    '''
    Import the data
    :param file: Type of file: prod,targets,cycle,lossTime
    :param debug: If true, will change the path (e.g. if running data_format.py directly)
    :param datatype: full or lite
    :return:
    '''
    if debug:
        path = '../data/'
    else:
        path = 'data/'
    if file == 'targets':
        return pd.read_excel(path + 'targets.xlsx')
    elif file == 'cycle':
        return pd.read_excel(path + 'eptTests_'+ datatype +'.xlsx')
    elif file == 'lossTime':
        return pd.read_excel(path + 'lossTime.xlsx')
    else:
        return pd.read_excel(path + 'production_'+datatype+'.xlsx')


def init_filter(data,weekends=False, mode = 'production'):
    '''
    Filter the data
    :param data:
    :param weekends: Include Saturday? (Sunday is always removed)
    :param mode: production or pass or ept or cycle (choose what checkpoint Codes to remove)
    :return:
    '''
    #Filter for correct checkpoint code
    if mode == 'production':
        data = data[data['checkpointCode']=='ET']
    elif mode == 'pass':
        data = data[data['checkpointCode'] != 'ET']
    elif mode == 'ept':
        data = data[data['checkpointCode'] == 'EP']
    elif mode == 'cycle':
        data.rename(columns = {'endShift':'shiftNumber'},inplace=True)

    data['date'] = pd.to_datetime(data['checkpointEndShiftDate'])
    data['weekday'] = data['date'].dt.dayofweek
    data['CW'] = pd.to_numeric(data['date'].dt.strftime("%V"))

    data = data[data['weekday'] != 6]
    # Remove weekends
    if weekends == False:
        data = data[data['weekday'] != 5]

    #Remove the new lines
    excl_lines = ['KSMX07','KSMX08','KSMX27','KUNDE']
    data = data[~data['productionLine'].isin(excl_lines)]

    return data

def remove_outliers(data):
    #Remove days with abnormally low production based on moving average
    data['ema'] = data.groupby(['productionLine'])['production'].apply(lambda x: x.ewm(span=5).mean())
    data['std'] = data.groupby(['productionLine'])['production'].apply(lambda x: x.rolling(5).std())
    # data['threshold'] = data['ema'] - 1*data['std']
    data['threshold'] = 0.25*data['ema']
    data['outlier'] = (data['production'] < data['threshold'])

    return data[data['outlier']!= True]

def regression_filt(data):
    #Remove AP5 harnesses for KSMX02,KSMX06 lines for better regression
    #Need to be updated whenever there is an engineering change
    data = data[~((data['productionLine'] == 'KSMX02') & (data['date'] <= datetime.datetime(2019,3,23)))]
    data = data[~((data['productionLine'] == 'KSMX06') & (data['date'] <= datetime.datetime(2019,2,25)))]
    data = data[~((data['productionLine'] == 'KSMX22') & (data['date'] >= datetime.datetime(2019, 4, 15)))]

    #Remove holidays
    excl_dates = [datetime.datetime(2019, 4, 19), datetime.datetime(2019, 4, 20),datetime.datetime(2019, 5, 27)]
    data = data[~data['date'].isin(excl_dates)]
    return data

def line_filter(data,lines):
    #Filter for specific lines
    if isinstance(lines,str):
        lines = [lines]
    return data[data['productionLine'].isin(lines)]

def shift_filter(data,shift):
    return data[data['shiftNumber']==shift]

def pivot(data):
    #Group data per day
    data = data.groupby(['date','productionLine','harnessTypeDescription','CW','checkpointCode'],as_index=False)['carNumber'].count()
    data.rename(columns = {'carNumber':'production'},inplace=True)
    data.sort_values(by=['date','productionLine'],inplace=True)
    data['numDays'] = data.groupby('productionLine').cumcount() + 1
    return data

def pivot_cycleTime(data):
    '''
    Group cycle time data per day
    :param data:
    :return:
    '''
    data = data.groupby(['date','origin','harnessTypeDescription','CW'],as_index = False)['eptMinutes'].agg(['sum','count','mean']).reset_index()
    data.sort_values(by=['date', 'origin'], inplace=True)
    return data

def pivot_ept(data):
    '''
    Group ept data per day
    :param data:
    :return:
    '''
    data = data.groupby(['date','origin','harnessTypeDescription','CW','checkpointCode'],as_index=False)['carNumber'].count()
    data.rename(columns = {'carNumber':'production'},inplace=True)
    data.sort_values(by=['date','origin'],inplace=True)
    return data

def get_lines(data,type='lines'):
    '''
    Get all unique lines (or ept tables) in order
    :param data:
    :param type:
    :return:
    '''
    if type == 'ept':
        data = data[data['origin'].str.startswith('ksmlpt',na=False)]
        return data['origin'].sort_values().unique()
    else:
        return data['productionLine'].sort_values().unique()

def past_production(data):
    '''
    Get past production and group by
    :param data:
    :return:
    '''
    return (data
            .groupby(['CW','productionLine','harnessTypeDescription'],as_index=False)['production']
            .mean()
            .sort_values(by=['productionLine']))

def pass_rate(data):
    '''
    Calculate total pass rate
    :param data:
    :return:
    '''
    data['total'] = data.groupby(['CW','origin','harnessTypeDescription','date'])['production'].transform(sum)
    data = data[data['checkpointCode'] == 'EP']
    data['passRate'] = data['production']/data['total']
    data.sort_values(by=['origin','date'],inplace=True)
    return data

if __name__ == '__main__':
    df = data_import(debug=True)
    df = init_filter(df,False)
    # df = line_filter(df,'KSMX26')
    df = shift_filter(df, 2)
    df = regression_filt(df)
    df = pivot(df)
    df = remove_outliers(df)
    # df = pass_rate(df)
    df.to_csv('test.csv')

