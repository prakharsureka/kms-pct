import pandas as pd
import time
import math

def custom_freq():
    weekmask = 'Mon Tue Wed Thu Fri Sat'
    custombday = pd.offsets.CustomBusinessDay(weekmask=weekmask)
    return custombday

def tick_freq(dates,max_ticks=8):
    '''
    Create evenly spaced ticks
    :param dates: range of dates
    :param max_ticks: Number of ticks shown
    :return:
    '''
    num_dates = len(dates)
    if num_dates > max_ticks:
        idx_length = math.ceil(num_dates/8)
        return dates[0::idx_length]
    else:
        return dates

def unixTimeMillis(dt):
    ''' Convert datetime to unix timestamp '''
    return int(time.mktime(dt.timetuple()))

def unixToDatetime(unix):
    ''' Convert unix timestamp to datetime. '''
    return pd.to_datetime(unix,unit='s')

def getMarks(start, end, Nth=100):
    ''' Returns the marks for labeling.
        Every Nth value will be used.
    '''
    daterange = pd.bdate_range(start=start, end=end,freq=custom_freq())
    result = {}
    for i, date in enumerate(daterange):
        if(i%Nth == 1):
            # Append value to dict
            result[unixTimeMillis(date)] = str(date.strftime('%b-%d (%a)'))

    return result